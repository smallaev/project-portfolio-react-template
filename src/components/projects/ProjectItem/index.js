import React, { useEffect, useState } from 'react';
import styled from "styled-components";
import theme from 'themes/main';

const columns = theme.layout.projects.columns;
const projectStyle = theme.layout.projects.projectStyle;
const responsiveBreakpoint = theme.layout.projects.responsiveBreakpoint;

const ProjectStyled = styled.section`
	width: ${100 / columns - (6 - columns)}%;
	margin: ${columns === 1 ? '0 auto 5em auto' : '0 0 5em 0'};
	background: rgba(0,0,0,.6);
	flex-shrink: 0;
	opacity: 0;
	transform: scale3d(1.2,1.2,1.2);
	transition: .5s all;

	${projectStyle === 'desc-side' && `	
		display: flex;
	`}
	${props => props.visible && `
		opacity: 1;
		transform: scale3d(1,1,1);
	`}
	@media (max-width: ${responsiveBreakpoint}) {
		width: 100%;
		flex-direction: column;
	}
`;

const Project = props => {
	const [visible, setVisible] = useState(false);

	useEffect(() => {
		setVisible(true);
	}, []);

	return <ProjectStyled visible={visible}>
		{props.children}
	</ProjectStyled>
}

export default Project;