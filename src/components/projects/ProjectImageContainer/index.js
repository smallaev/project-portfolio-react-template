import React, { useEffect, useState } from 'react';
import styled from "styled-components";
import theme from 'themes/main';


const projectStyle = theme.layout.projects.projectStyle;
const responsiveBreakpoint = theme.layout.projects.responsiveBreakpoint;

const ProjectImageContainerStyled = styled.div`
	position: relative;
	max-height: 300px;
	overflow: hidden;
	opacity: 0;
	transition: .6s all;
	transform: translateY(-5px);
	${projectStyle === 'desc-side' && `	
		width: 50%;
	`}
	@media (max-width: ${responsiveBreakpoint}) {
		width: 100%;
	}
	${props => props.visible && `
		opacity: 1;
		transform: translateY(0);
	`}
`;

const ProjectImageContainer = props => {
	const [visible, setVisible] = useState(false);

	useEffect(() => {
		setVisible(true);
	}, []);

	return <ProjectImageContainerStyled visible={visible}>
			{props.children}
		</ProjectImageContainerStyled>
}

export default ProjectImageContainer;