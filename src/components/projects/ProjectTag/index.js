import React, { useEffect, useState } from 'react';
import styled from "styled-components";
import theme from 'themes/main';

const ProjectTagStyled = styled.div`
		font-size: 15px;
		margin: 0 .4em .2em 0;
		display: inline-block;
		font-weight: normal;
		color: ${theme.colors.textColor};
		border: 1px solid ${theme.colors.textColor};
		padding: 0em .4em .1em .4em;
		border-radius: 8px;
		position: relative;
		opacity: 0;
		transform: translate(-5px);
		transition: .5s all;
		transition-delay: ${props => props.delay + 'ms'};

		${props => props.visible && `
			opacity: 1;
		    transform: translate(0);
		`}
`;

const ProjectTag = props => {
	const [visible, setVisible] = useState(false);

	useEffect(() => {
		setVisible(true);
	}, []);

	return <ProjectTagStyled visible={visible} delay={props.delay}>
		{props.children}
	</ProjectTagStyled>
}

export default ProjectTag;