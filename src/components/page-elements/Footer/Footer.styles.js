import styled from 'styled-components';
import theme from 'themes/main';

export const FooterStyled = styled.footer`

`;

export const FooterInner = styled.div`
    max-width: 1200px;
    padding: 2em;
    margin: 0 auto;
    box-sizing: border-box;
    font-size: 15px;
`;

export const FooterLinksList = styled.ul`
	list-style-type: none;
	margin: 0;
	padding: 0;
	display: flex;
    justify-content: center;
`;

export const FooterLinksItem = styled.li`
    margin: 0 .5em;
`;

export const FooterLinksLink = styled.a`
	text-decoration: none;
    color: ${theme.colors.accentColor};
    transition: .2s all;

	&:hover {
		opacity: .6;
	}
`;