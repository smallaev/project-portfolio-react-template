import React from 'react';
import { 
	FooterStyled, 
	FooterInner, 
	FooterLinksList, 
	FooterLinksItem, 
	FooterLinksLink, 
} from './Footer.styles';


const Footer = () => {
	return (
		<FooterStyled>
			<FooterInner>
				<FooterLinksList>
					<FooterLinksItem>
						<FooterLinksLink href="mailto:smallaev@outlook.com">
							Contact
						</FooterLinksLink>
					</FooterLinksItem>
					<FooterLinksItem>
						<FooterLinksLink href="https://www.linkedin.com/in/smallaev" target="_blank">
							LinkedIn
						</FooterLinksLink>
					</FooterLinksItem>
					<FooterLinksItem>
						<FooterLinksLink href="https://gitlab.com/smallaev" target="_blank">
							GitLab
						</FooterLinksLink>
					</FooterLinksItem>
				</FooterLinksList>
			</FooterInner>
		</FooterStyled>
	);
}

export default Footer;