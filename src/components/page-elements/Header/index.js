import React, { useState } from 'react';
import UnderlinedLink from 'components/UnderlinedLink';
import {
	HeaderStyled,
	HeaderInner,
	HeaderLogoContainer,
	HeaderLogoLink,
	HeaderNavbar,
	HeaderNavbarList,
	HeaderNavbarItem,
	HeaderNavbarToggle
} from './Header.styles';


const HeaderLink = props => {
	return <UnderlinedLink to={props.to} fontSize="24px">
		{props.children}
	</UnderlinedLink>
}

const Header = () => {
	const [navbarActive, setNavbarActive] = useState(false);

	const onToggleClick = (e) => {
		e.preventDefault();
		setNavbarActive(!navbarActive);
	}

	return (
		<HeaderStyled>
			<HeaderInner>
				<HeaderLogoContainer>
					<HeaderLogoLink to="/">
						<img src="images/logo.png" alt="logo" width="30" />
					</HeaderLogoLink>
				</HeaderLogoContainer>
				<HeaderNavbar>
					<HeaderNavbarList active={navbarActive}>
						<HeaderNavbarItem>
							<HeaderLink to="/">
								Home
							</HeaderLink>
						</HeaderNavbarItem>
						<HeaderNavbarItem>
							<HeaderLink to="projects">
								Projects
							</HeaderLink>
						</HeaderNavbarItem>
					</HeaderNavbarList>
					<HeaderNavbarToggle active={navbarActive} onClick={onToggleClick} href="#">
						<span></span>
					</HeaderNavbarToggle>
				</HeaderNavbar>
			</HeaderInner>
		</HeaderStyled>
	);
}

export default Header;