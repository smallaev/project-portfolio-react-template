import styled from 'styled-components';
import { Link } from 'react-router-dom';
import theme from 'themes/main';

export const HeaderStyled = styled.header`

`;

export const HeaderInner = styled.div`
	padding: 2em 2.5em;
    max-width: 1200px;
    margin: 0 auto;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: space-between;
    overflow: hidden;

	@media (max-width: 600px) {
		padding: 2em 1em;
	}
`;

export const HeaderLogoContainer = styled.div`
	display: flex;
`;

export const HeaderLogoLink = styled(Link)`
	font-size: 0;
	line-height: 0;
`;

export const HeaderNavbar = styled.nav`
	display: flex;
	align-items: center;
`;

export const HeaderNavbarList = styled.ul`
	list-style-type: none;
    margin: 0;
    padding: 0;
    display: flex;
    justify-content: center;
    opacity: 0;
    transform: translate(100px) scaleX(0);
    transition: .2s all;

	${props => props.active && `
		opacity: 1;
		transform: translate(0) scaleX(1);
	`}
`;

export const HeaderNavbarItem = styled.li`
    margin: 0 .5em;
`;

export const HeaderNavbarToggle = styled.a`
	position: relative;
    width: 30px;
    height: 20px;
    display: block;
    margin-top: .2em;
    margin-left: 1em;

	& span,
	&::before,
	&::after {
		display: block;
		height: 1px;
		width: 100%;
		left: 0;
		background-color: ${theme.colors.accentColor};
		transform-origin: 2px 100%;
		transition: .4s all;
	}

	& span {
		top: 50%;
		margin-top: -1px;
		position: relative;

		${props => props.active && `
			opacity: 0;
    		transform: translate(50px);
		`}
	}

	&::before {
		content: "";
		position: absolute;
		top: 0;
		
		${props => props.active && `
		    transform: rotate(45deg);			
		`}
	}

	&::after {
		content: "";
		position: absolute;
		bottom: 0;

		${props => props.active && `
    		transform: rotate(-45deg);		
		`}
	}
`;