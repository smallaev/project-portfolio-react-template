import React from 'react';
import styled from 'styled-components';

const MainStyled = styled.main`
	max-width: 1200px;
    padding: 0 2.5em;
    margin: 0 auto;
    box-sizing: border-box;

	@media (max-width: 800px) {
		padding: 0 1em;
	}
`;

const Main = (props) => {
	return (
		<MainStyled>
			{props.children}
		</MainStyled>
	);
}

export default Main;