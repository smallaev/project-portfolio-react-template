import styled from 'styled-components';

export const PageLayout = styled.div`
    display: flex;
    min-height: 100vh;
    flex-direction: column;
	justify-content: space-between;
	padding: 0 1em;
	overflow-x: hidden;
	position: relative;

	@media (max-width: 600px) {
		padding: 0;
	}
`;

export const PageBackground = styled.div`
	z-index: -1;
	position: absolute;
	top: 0;
	left: -120%;
	width: 150vw;
	height: 100%;
	background-color: rgba(0, 0, 0, .2);
	transform-origin: 0 0;
	transform: skew(10deg);
`;