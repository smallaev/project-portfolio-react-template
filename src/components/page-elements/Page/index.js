import React from 'react';
import Header from 'components/page-elements/Header';
import Footer from 'components/page-elements/Footer';
import Main from 'components/page-elements/Main';
import {
	PageLayout,
	PageBackground
} from './Page.styles';


const Page = (props) => {
	return (
		<PageLayout>
			<PageBackground />
            <Header />
            <Main>
                {props.children}
            </Main>
            <Footer />
		</PageLayout>
	);
}

export default Page;