import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Word } from "components/WordsTransition/Word";

const WordsStyled = styled.div`
	${props => props.skew && `	
		transform: skew(15deg);
	`}
	@media (max-width: 600px) {
		transform: skew(0);
	}
`;

export const WordsTransition = props => {
	let wordsRet = [];
	let words = props.children;
	if (words) {
		words = words.split(' ');

		if (words.length > 1) {
			let delay = 0;
			wordsRet = words.map(word => {
				delay += 30;
				return <Word delay={delay}>{word}</Word>;
			});
		}
	}

	return <WordsStyled skew={props.skew}>
		{wordsRet}
	</WordsStyled>;
}