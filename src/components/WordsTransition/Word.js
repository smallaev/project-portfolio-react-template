import React, { useEffect, useState } from "react";
import styled from "styled-components";

const WordStyled = styled.span`
	display: inline-block;
	opacity: 0;
	transform: translate(15px);
	margin-right: .5ch;
	transition: .5s all;
	transition-delay: ${props => props.delay + 'ms'};

	&:last-of-type {
		margin-right: 0;
	}

	${props => props.visible && `
		opacity: 1;
		transform: translate(0);
	`}
`;

export const Word = props => {
	const [visible, setVisible] = useState(false);

	useEffect(() => {
		setVisible(true);
	}, []);

	return <WordStyled visible={visible} delay={props.delay}>
		{props.children}
	</WordStyled>
}