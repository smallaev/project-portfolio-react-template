import React, { useEffect, useState } from 'react';
import Page from 'components/page-elements/Page';
import { WordsTransition } from 'components/WordsTransition';
import UnderlinedLink from 'components/UnderlinedLink';
import {
	ProjectList,
	ProjectImage,
	ProjectImageOverlay,
	ProjectImageOverlayText,
	ProjectTitle,
	ProjectSubtitle,
	ProjectTextContainer,
	ProjectContent,
} from './ProjectsPage.styles';
import ProjectItem from 'components/projects/ProjectItem';
import ProjectTag from 'components/projects/ProjectTag';
import ProjectImageContainer from 'components/projects/ProjectImageContainer';
import projects from './projects.json';


const ProjectsPage = () => {
	const [translateValue, setTranslateValue] = useState(0);

	const handleProjectsScroll = (e) => {
		e.preventDefault();
		if (e.deltaY && e.deltaY > 0) {
			setTranslateValue(translateValue-1);
		}
		if (e.deltaY && e.deltaY < 0) {
			setTranslateValue(translateValue+1);
		}
	}

	return (
		<Page>
			<ProjectList translate={translateValue}>
				{projects.map(project => {
					let delay = 0;
					return <ProjectItem>
						<ProjectImageContainer>
							<ProjectImage src={`images/projects/wide/${project.image}`} alt={project.title} />
							<ProjectImageOverlay>
								<ProjectImageOverlayText>
									
								</ProjectImageOverlayText>
							</ProjectImageOverlay>
						</ProjectImageContainer>
						<ProjectTextContainer>
							<ProjectTitle>{project.title}</ProjectTitle> 
							<ProjectSubtitle>{project.subtitle}</ProjectSubtitle>
							<div>
								{project.tags.map(tag => {
									delay += 30;
									return <ProjectTag delay={delay}>{tag}</ProjectTag>
								})}
							</div>
							<ProjectContent>
								<WordsTransition skew={false}>
									{project.content}
								</WordsTransition>
							</ProjectContent>
							<div>
								<UnderlinedLink to={{ pathname: project.link }} target="_blank" fontSize="20px">
									<WordsTransition skew={false}>									
										View Project
									</WordsTransition>
								</UnderlinedLink>
							</div>
						</ProjectTextContainer>
					</ProjectItem>
				})}
			</ProjectList>
		</Page>
	)
};

export default ProjectsPage;