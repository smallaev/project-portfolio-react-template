import styled from 'styled-components';
import theme from 'themes/main';


const projectStyle = theme.layout.projects.projectStyle;
const responsiveBreakpoint = theme.layout.projects.responsiveBreakpoint;

export const ProjectList = styled.section`
	padding: 1em 0;
	margin-top: 1em;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
	perspective: 1500px;
	${props => props.translate && `
		transform: translate3d(${props.translate}vw, 0, 0)
	`}
`;

export const ProjectImage = styled.img`
    max-width: 100%;
	position: relative;
	display: block;
`;

export const ProjectImageOverlay = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background: rgba(0, 0, 0, .6);
`;

export const ProjectImageOverlayText = styled.div`
	position: absolute;
	bottom: 0;
	left: 0;
	padding: 1em;
`;

export const ProjectTitle = styled.h1`
	margin: 0;
    font-size: 24px;
	font-weight: 400;
`;

export const ProjectSubtitle = styled.h2`
	margin: 0;
    font-size: 20px;
    font-weight: 300;
	line-height: 1.2;
	margin-bottom: .5em;
`;

export const ProjectTextContainer = styled.div`
	padding: 1em;
	${projectStyle === 'desc-side' && `	
		width: 50%;
	`}
	@media (max-width: ${responsiveBreakpoint}) {
		padding: 1em;
		width: 100%;
	}
`;

export const ProjectContent = styled.div`
	margin-top: .5em;
	margin-bottom: 1em;
`;