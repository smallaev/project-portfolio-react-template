import styled from 'styled-components';
import theme from 'themes/main';

export const HomePageInfo = styled.div`
	margin: 2em auto;
	max-width: 600px;
	border-right: 1px solid ${theme.colors.textColor};
	text-align: right;
	padding: 1em;
	transform: skew(-15deg);

	@media (max-width: 600px) {
		transform: skew(0);
	}
`;

export const HomePageName = styled.h1`
    font-family: 'Barlow Condensed', sans-serif;
    margin: 0;
	font-size: 30px;
`;

export const HomePageTitle = styled.h2`
    font-family: 'Barlow Condensed', sans-serif;
    color: ${theme.colors.accentColor};
    font-size: 20px;
    margin: 0 0 1em 0;
`;

export const HomePageAbout = styled.div`
    max-width: 600px;
	margin-bottom: 1em;
`;