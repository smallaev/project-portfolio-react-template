import React, { useState, useEffect } from 'react';
import Page from 'components/page-elements/Page';
import { WordsTransition } from 'components/WordsTransition';
import { 
	HomePageAbout, 
	HomePageInfo, 
	HomePageName, 
	HomePageTitle 
} from './HomePage.styles';
import UnderlinedLink from 'components/UnderlinedLink';

const HomePage = () => {
	return (
		<Page>
			<HomePageInfo>
				<div>
					<HomePageName>
						<WordsTransition skew={true}>
							Shamil Mallaev
						</WordsTransition>
					</HomePageName>
					<HomePageTitle>
						<WordsTransition skew={true}>
							Full stack developer
						</WordsTransition>
					</HomePageTitle>
				</div>
				<HomePageAbout>
					<WordsTransition skew={true}>
						Passionate developer with several years of experience building
					</WordsTransition>
					<WordsTransition skew={true}>
						bespoke websites, web apps and portals with thousands of monthly visitors
					</WordsTransition>
				</HomePageAbout>
				<div>
					<UnderlinedLink to="projects" fontSize="24px">
						<WordsTransition skew={true}>
							View Projects
						</WordsTransition>
					</UnderlinedLink>
				</div>
			</HomePageInfo>
		</Page>
	)
};

export default HomePage;