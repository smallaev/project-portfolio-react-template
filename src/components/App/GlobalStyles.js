import { createGlobalStyle } from 'styled-components';
import theme from 'themes/main';

const GlobalStyles = createGlobalStyle`

	body {
		background-color: ${theme.colors.bgPrimary};
		color: ${theme.colors.textColor};
		font-family: 'Barlow Condensed', sans-serif;
		font-size: 20px;
		line-height: 1.5;
		font-weight: 300;
		margin: 0;
		padding: 0;
	}

	h1, h2, h3 {
		font-weight: 400;
	}
`;

export default GlobalStyles;