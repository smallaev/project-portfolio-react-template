import React from 'react';
import {
	BrowserRouter as Router,
	Switch,
	Route
} from 'react-router-dom';
import HomePage from 'components/pages/HomePage';
import ProjectsPage from 'components/pages/ProjectsPage';
import GlobalStyles from './GlobalStyles';


const App = () => {
	return (<>
		<GlobalStyles />
		<Router>
			<Switch>
				<Route path="/" exact={true}>
					<HomePage />
				</Route>
				<Route path="/projects" exact={true}>
					<ProjectsPage />
				</Route>
			</Switch>
		</Router>
	</>);
}

export default App;