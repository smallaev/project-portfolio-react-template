import styled from 'styled-components';
import { Link } from 'react-router-dom';
import theme from 'themes/main';

const UnderlinedLink = styled(Link)`
	color: ${theme.colors.accentColor};
    text-decoration: none;
    position: relative;
    padding: .2em 0;
    margin: 0;
    display: inline-block;
    font-size: ${props => props.fontSize};

	&::after {
		position: absolute;
		display: block;
		content: "";
		left: 0;
		bottom: 0;
		width: 0;
		height: 1px;
		background-color: ${theme.colors.accentColor};
		transition: .4s all;
	}

	&:hover::after {
		width: 100%;
	}
`;

export default UnderlinedLink;