export default {
	colors: {
		bgPrimary: '#404963',
		textColor: '#fff',
		accentColor: '#e0d05a'
	},
	layout: {
		projects: {
			columns: 2,
			projectStyle: 'desc-bottom', // desc-bottom | desc-side
			responsiveBreakpoint: '900px'
		}
	}
}