# Portfolio template built with React JS

This template is based on [Create React App](https://github.com/facebook/create-react-app). It includes a set of React components and home and project page templates.

Example: https://smallaev-web.com

## Available Scripts

In the project directory, run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.